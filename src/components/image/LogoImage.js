import React, {useState, useEffect} from 'react'
import {Text, View, Dimensions, StyleSheet, Image, ScrollView , TouchableOpacity } from 'react-native';
import Colors from '../../style/Colors';

const {width} = Dimensions.get('window');

const LogoImage = (props) => {

    return (
        <View style={styles.container} >
            <Image source={require('../../../assets/logo.png')} style={styles.logo} />
            <View style={styles.chanceContainer} >
                <Image source={require('../../../assets/chance.png')} style={styles.chance} />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        width: width,
        height: 300,
        alignItems: 'center',
        borderBottomLeftRadius: 25,
        borderBottomRightRadius: 25,
        backgroundColor: Colors.LIGHT_BLUE,
    },
    chanceContainer: {
        position: 'absolute',
        bottom: 0,
    },
    buttonText: {
        fontSize: 16,
        lineHeight: 22,
        fontWeight: 'bold',
        color: Colors.BLACK
    },
    logo: {
        height: 300,
        resizeMode: 'contain'
    },
    chance: {
        height: 100,
        resizeMode: 'contain'
    },
})


export default LogoImage