import React, {useState, useEffect} from 'react'
import {Text, View, Dimensions, StyleSheet, Image, ScrollView , TouchableOpacity } from 'react-native';
import Colors from '../../style/Colors';

const {width} = Dimensions.get('window');

const Button = (props) => {

    const { title, onPress } = props; 

    return (
        <TouchableOpacity activeOpacity={0.4} onPress={onPress} style={styles.button} >
            <Text style={styles.buttonText} >{title}</Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    button: {
        width: 120,
        height: 50,
        borderRadius: 8,
        marginRight: 10,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors.BLUE
    },
    buttonText: {
        fontSize: 16,
        lineHeight: 22,
        fontWeight: 'bold',
        color: Colors.WHITE
    }
})


export default Button