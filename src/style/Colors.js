const Colors = {
    RED: '#FF0000',
    BLACK: '#000000',
    WHITE: '#FFFFFF',
    GRAY: '#A9A9A9',
    YELLOW: '#f2b443',
    PURPLE: '#6D5EF6',
    CREAM: '#fcd5aa',
    ORANGE: '#fa882d',
    BLUE: '#87b3e6',
    LIGHT_BLUE: '#eefafd',
    LIGHT_ORANGE: '#fda25b',
};

export default Colors;