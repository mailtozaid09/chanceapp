import React, {useState, useEffect} from 'react'
import {Text, View, Dimensions, StyleSheet, TextInput, ScrollView , TouchableOpacity } from 'react-native';
import Button from '../components/button/Button';
import OutlineButton from '../components/button/OutlineButton';
import LogoImage from '../components/image/LogoImage';
import Colors from '../style/Colors';

const {width} = Dimensions.get('window');

const LoginScreen = () => {

    const heading = "The personality first connecting app";
    const subHeading = "If you were a fruit, then you'd be a fine-apple";



    return (
        <View style={styles.mainContainer} >
            <View style={styles.container} >
                <LogoImage title="Ds" />
                <View style={styles.textContainer} >
                    <Text style={styles.heading}>{heading}</Text>
                    <Text style={styles.subHeading}>{subHeading}</Text>
                </View>
            </View>
            
            <View style={styles.buttonContainer} >
                <Button title="Login" />
                <OutlineButton title="Sign Up" />
            </View>
            
        </View>
    )
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: Colors.WHITE,
    },
    buttonContainer: {
        marginBottom: 40,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    textContainer: {
        padding: 15,
        paddingTop: 50,
    },
    container: {
        alignItems: 'center',
    },
    heading: {
        fontSize: 40,
        lineHeight: 40,
        fontWeight: 'bold',
        color: Colors.BLACK,
    },
    subHeading: {
        fontSize: 18,
        lineHeight: 22,
        marginTop: 30,
        fontWeight: '400',
        textAlign: 'center',
        color: Colors.BLACK,
    },
})

export default LoginScreen